from flask import Blueprint, request, jsonify
from app.models.anime_model import animes
from psycopg2 import errors

bp_animes = Blueprint("bp_anime_id_route", __name__)

@bp_animes.route("/animes/<int:anime_id>")
def get_create(anime_id):
    anime = animes()
    try:
        output = anime.filter(anime_id)
        output["released_date"] = output["released_date"].strftime('%d/%m/%Y')
        jsonify(output)
        return {"data": [output]}, 200
    except:
        return {"error": "Not Found"}

@bp_animes.route("/animes/<int:anime_id>", methods=["PATCH"])
def update(anime_id: int):
    anime = animes()
    data = request.get_json()
    try:
        return anime.update_anime(data, anime_id)
    except KeyError as e:
        return e.args[0], 422

        



@bp_animes.route("/animes/<int:anime_id>", methods=["DELETE"])
def delete(anime_id: int):
    anime = animes()
    try:
        return anime.delete(anime_id), 204
    except:
        return {"error": "not found"}

