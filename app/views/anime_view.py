from flask import Blueprint, request, jsonify
from app.models.anime_model import animes
from psycopg2 import errors

bp_animes = Blueprint("bp_anime_route", __name__)


@bp_animes.route("/animes")
def get_create():
    anime = animes()
    try:
        data = jsonify({"data": anime.pegar_dados()})
        return  data, 200
    except:
        return {"data": []}, 200


@bp_animes.route("/animes", methods=["POST"])
def post_create():
    anime = animes()
    data = request.get_json()
    try:
        return anime.create_anime(data), 201
    except errors.UniqueViolation as _:
        return {"error": "anime is already exists" }, 422
    except KeyError as e:
        return e.args[0], 422