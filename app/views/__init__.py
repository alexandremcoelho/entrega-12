from flask import Flask

from .anime_view import bp_animes as bp_anime
from .anime_id_view import bp_animes as bp_anime_id


def init_app(app: Flask):
    app.register_blueprint(bp_anime)
    app.register_blueprint(bp_anime_id)


