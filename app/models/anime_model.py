from . import conn_cur, end_conn_cur

class animes:
    table_fields = ["id", "anime", "released_date", "seasons"]
    required_fields = ["anime", "released_date", "seasons"]

    def pegar_dados(self):
        conn, cur = conn_cur()

        cur.execute("SELECT * FROM animes")

        query = cur.fetchall()

        end_conn_cur(conn, cur)
        
        output = [dict(zip(self.table_fields, item)) for item in query]
        for item in output:
            item["released_date"] = item["released_date"].strftime('%d/%m/%Y')
        return output

    

    @staticmethod
    def create_table():
        conn, cur = conn_cur()
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS animes
            (
                id BIGSERIAL PRIMARY KEY,
                anime VARCHAR NOT NULL UNIQUE,
                released_date DATE NOT NULL,
                seasons INTEGER
            )
            """
        )
        end_conn_cur(conn, cur)


    def filter(self, anime_id):
        try:
            conn, cur = conn_cur()
            self.create_table()
            cur.execute(
            """
                SELECT *
                FROM
                    animes a
                WHERE
                    a.id = %(anime_id)s
            """,
                {"anime_id": anime_id},
            )

            query = cur.fetchone()
            if not query:
                return {"error": "not found"}

            end_conn_cur(conn, cur)

            resposta = dict(zip(self.table_fields, query))

            return resposta
        except:
            return []

       


    def check_fields(self, data: dict):
        recived_keys = data.keys()
        return [
            field
            for field in recived_keys
            if field not in self.required_fields 
        ]

    
    def create_anime(self, data:dict):
        conn, cur = conn_cur()
        self.create_table()
        check_fields = self.check_fields(data)

        if check_fields:
            raise KeyError(
                {
                    "available_keys": self.required_fields,
                    "wrong_keys_sended": check_fields,
                }
            )

        data["anime"] = data["anime"].title()
        cur.execute(
            """
                INSERT INTO animes
                    (anime, released_date, seasons)
                VALUES
                    (%(anime)s, %(released_date)s, %(seasons)s)
                RETURNING *
            """,
            data,
        )
        query = cur.fetchone()

        end_conn_cur(conn, cur)
        output = dict(zip(self.table_fields, query))
        output["released_date"] = data["released_date"]
        return output




    def update_anime(self, data: dict, product_id: int):
        conn, cur = conn_cur()
        
        check_fields = self.check_fields(data)
        if check_fields:
            raise KeyError(
                {
                    "available_keys": self.required_fields,
                    "wrong_keys_sended": check_fields,
                }
            )

        new_data = self.filter(product_id)
        print(new_data)
        if new_data == {"error": "not found"}:
            return new_data, 404
        
        if data["anime"]:
            data["anime"] = data["anime"].title()
        for item in data:
            new_data[item] = data[item]
        
        cur.execute(
            """
                UPDATE animes
                SET anime = %(anime)s
                    WHERE id = %(id)s
                RETURNING *
            """,
            new_data,
        )

        query = cur.fetchone()

        end_conn_cur(conn, cur)
        new_data["released_date"] = new_data["released_date"].strftime('%d/%m/%Y')

        return new_data

    
    def delete(self, anime_id: int):
        
        conn, cur = conn_cur()
        cur.execute(
            """
                DELETE FROM animes
                    WHERE animes.id = %(anime_id)s
            """,
            {"anime_id": anime_id},
        )
        end_conn_cur(conn, cur)
        return "No content"
        
